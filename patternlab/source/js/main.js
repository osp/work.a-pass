// document.querySelectorAll('.js-toggle').forEach(function(elt, index) {
//     elt.addEventListener("click", function() {
//         this.closest('.panel').classList.toggle('is-collapsed');
//     });
// })

document.querySelectorAll('.js-filter').forEach(function(elt, index) {
  elt.addEventListener("click", function() {
    var keyword = this.dataset.keyword;

    document.querySelectorAll('.article_entry').forEach(function(elt, index) {
      elt.classList.add('is-hidden');
    });

    document.querySelectorAll('.article_entry[data-keyword~="' + keyword + '"]').forEach(function(elt, index) {
      elt.classList.remove('is-hidden');
    });
  });
})

document.querySelectorAll('.js-toggle').forEach(function(elt, index) {
  elt.addEventListener("click", function() {
    var currentTarget = this.closest('.panel').dataset.target;
    if (this.dataset.target == currentTarget) {
      this.classList.remove('active');
      this.closest('.panel').dataset.target = null;
    } else {
      var target = this.dataset.target;
      this.closest('.panel').dataset.target = target;
    }
  });
})
