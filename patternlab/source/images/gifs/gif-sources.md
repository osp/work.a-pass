## archive-1.gif
File name : Twisting somersault.gif
Source : E. Muybridge
User : Racconish
Link : https://commons.wikimedia.org/wiki/Category:Eadweard_Muybridge_animations#/media/File:Twisting_somersault.gif
License : Public domain, CC0

## book-1.gif
File name : spintriang.gif
Author : Loshadka
Link : http://www.loshadka.org/wp/?p=1463
License : ?

## collection-1.gif
File name : Descriptive Zoopraxography Greyhound Galloping Animated 12.gif
Date : 1 January 1893
Author : Edward James Muggeridge
Link : https://commons.wikimedia.org/wiki/Category:Eadweard_Muybridge_animations#/media/File:Descriptive_Zoopraxography_Greyhound_Galloping_Animated_13.gif
License : Public domain, CC0

## diary-1.gif
File name : Algol-type_variable_binary_star_animation_1.gif
Date : 15 May 2019
Author : Merikanto
License : CC-BY-SA
Link : https://commons.wikimedia.org/wiki/Category:Eclipsing_binary_stars#/media/File:Algol-type_variable_binary_star_animation_1.gif

## document-1.gif
File name : 240px-3d_Scapula_Rendered.gif
Author : Doctor Jana
Link : 	https://docjana.com/scapula-3d-anatomy-prototype/
License : CC BY-SA

## draft-1.gif
File name : 800px-Animated_3_Gear_Row.gif
Author : Jahobr
Link : https://commons.wikimedia.org/wiki/File:Animated_3_Gear_Row_Frame1.svg
License : Public domain, CC0

## drawing-1.gif
File name : Alpha_animiert_2018.gif
Author : Thirunavukkarasye-Raveendran
Link : https://upload.wikimedia.org/wikipedia/commons/a/a1/Alpha_animiert_2018.gif
License : CC BY-SA

## fable-1.gif
File name : Elephant Walking animated.gif
Author : Edward James Muggeridge
Source : https://commons.wikimedia.org/wiki/File:Elephant_Walking_animated.gif
License : Public domain, CC0

## fiction-1.gif
File name : Eclipse lunar 2019.gif
Link : https://commons.wikimedia.org/wiki/File:Eclipse_lunar_2019.gif
Author : Caroline Grubb
License : CC BY

## letter-1.gif
File name : giphy.gif
Author : Laurène Boglio
Link : https://tenor.com/view/pigeons-seduction-harassment-boglio-laurene-boglio-gif-13857714
License : ?

## library-1.gif
File name : Cat trotting, changing to a gallop.gif
Author : François de Dijon
Link : https://commons.wikimedia.org/wiki/File:Cat_trotting,_changing_to_a_gallop.gif
License : Public domain, CC0

## list-1.gif
File name :
Author :
Link : https://gifer.com/en/VooF
License :

## litterature-1.gif
File name : Descriptive Zoopraxography Pigeons Flying Animated 12.gif
Author : E. Muybridge
Link : https://commons.wikimedia.org/wiki/File:Descriptive_Zoopraxography_Pigeons_Flying_Animated_12.gif
License : Public domain, CC0

## manifesto-1.gif
File name : Electricity Test short animmation
Author : smault23
Link : https://www.deviantart.com/smault23/art/Electricity-Test-short-animmation-610088750
License : Creative Commons Attribution-Noncommercial-No Derivative Works 3.0 License

## meeting-1.gif
File name : Kiss, animated from Animal locomotion, Vol. IV, Plate 444 by Eadweard Muybridge.gif
Date : 20 June 2012
Source : E. Muybridge
User : TimofKingsland
Link : https://en.wikipedia.org/wiki/File:Kiss,_animated_from_Animal_locomotion,_Vol._IV,_Plate_444_by_Eadweard_Muybridge.gif
License : Public domain, CC0

## meeting-2.gif
File name : Asymmetric Epicyclic Gearing Stationary.svg
Author : Jahobr
Link : https://upload.wikimedia.org/wikipedia/commons/2/29/Asymmetric_Epicyclic_Gearing_Stationary_Ring.gif
License : Creative Commons CC0 1.0 Universal Public Domain Dedication

## meeting-3.gif
File name :
Author :
Link :
License :

## message-1.gif
File name : Planetary Gear Animation.gif
Author : Laserlicht
Link : https://commons.wikimedia.org/wiki/File:Planetary_Gear_Animation.gif
License :  Creative Commons CC0 1.0 Universal Public Domain Dedication

## manual-1.gif
File name : Spiral spring animation.gif
Author : Leungcwd
Link : https://commons.wikimedia.org/wiki/Category:Animations_of_spirals#/media/File:Spiral_spring_animation.gif
License : CC BY-SA 3.0

## manual-2.gif
File name : Sundial Sun Clock GIF
Author : echefede
Link : https://tenor.com/view/sundial-sun-clock-wrist-watch-biased-bias-gif-11795379
License : MIT
Sundial Sun Clock GIF

## non-fiction.gif
File name : Clock reversed.gif/File:Clock_reversed.gif
Author : Janka
Link : https://commons.wikimedia.org/wiki/File:Clock_reversed.gif
License :  Creative Commons Attribution-Share Alike 3.0

## notes-1.gif
File name : Animhorse.gif
Author :  J-E Nyström
Link : https://en.wikipedia.org/wiki/Animation#/media/File:Animhorse.gif
License : CC BY-SA 2.5

## poetry-1.gif
File name : Time-lapse II
Author :  luisbc
Link : https://www.deviantart.com/luisbc/art/Time-lapse-II-355115510

## report-1.gif
File name : Point Light Display of ASL sentence.gif
Author :  Athena.PEN
Link : https://upload.wikimedia.org/wikipedia/commons/3/3b/Point_Light_Display_of_ASL_sentence.gif
License : Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0

## score-1.gif
File name : Animated Diagonal Scale.gif
Author : Sandra Hooghwinkel
Link : https://commons.wikimedia.org/wiki/Category:Laban_Movement_Analysis#/media/File:Animated_Diagonal_Scale.gif
License : CC BY-SA 4.0

## sticky-1.gif
File name : Smile 3.gif
Author : JenVan
Link : https://commons.wikimedia.org/wiki/File:Smile_3.gif
License : public domain

## scribble-1.gif
File name : Knot Unfolding.gif
Author : Kuchtact
Link : https://commons.wikimedia.org/wiki/File:Knot_Unfolding.gif
License : Creative Commons Attribution-Share Alike 4.0

## theory-1.gif
File name : Penrose-triangle-4color-rotation.gif
Author : Tomruen
Link : https://thereaderwiki.com/en/Penrose_triangle
License : Creative Commons Attribution-Share Alike 4.0 International license.


## transcript-1.gif
File name : Animated collotype Boys playing Leapfrog
Author : Xpicto
Link : https://en.wikipedia.org/wiki/Eadweard_Muybridge#/media/File:Eadweard_Muybridge_Boys_playing_Leapfrog_(1883%E2%80%9386,_printed_1887)_animated_A.gif
License : CC BY-SA 4.0

## tutorial-1.gif
File name : A trefoil knot
Author : Jim.belk
Link : https://en.wikipedia.org/wiki/Trefoil_knot#/media/File:Blue_Trefoil_Knot_Animated.gif
License : Public Domain
