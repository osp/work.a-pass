#! /usr/bin/env bash 

# Copy pattern folder and change working directy
rm -fr patterns.django
cp -r source/_patterns patterns.django
cd patterns.django

# Fix include and extends paths
find . -name '*.twig' -print0 | xargs -0 sed -i 's/@/apass\//g'

# Fix with tag syntax
find . -name '*.twig' -print0 | xargs -0 sed -i 's/with {\([^}]*\)}/with \1/g'
# FIXME: removes the : from http:// href
find . -name '*.twig' -print0 | xargs -0 sed -i 's/: */=/g'
find . -name '*.twig' -print0 | xargs -0 sed -i 's/,\( [^=]*=\)/\1/g'

# fix filters syntax
find . -name '*.twig' -print0 | xargs -0 sed -i 's/date("d\.m\.Y")/date:"d.m.Y"/g'
find . -name '*.twig' -print0 | xargs -0 sed -i 's/|raw/|safe/g'

# fix urls
find . -name '*.twig' -print0 | xargs -0 sed -i "s/{{ routes.index }}/{% url 'home' %}/g"
find . -name '*.twig' -print0 | xargs -0 sed -i "s/{{ routes.padDelete }}/{% url 'pad-delete' %}/g"
find . -name '*.twig' -print0 | xargs -0 sed -i "s/{{ routes.padManage }}/{% url 'manage' %}/g"
find . -name '*.twig' -print0 | xargs -0 sed -i "s/{{ routes.padPublish }}/{% url 'pad-public' %}/g"
find . -name '*.twig' -print0 | xargs -0 sed -i "s/{{ routes.padRead }}/{% url 'pad-read' %}/g"
find . -name '*.twig' -print0 | xargs -0 sed -i "s/{{ routes.padRename }}/{% url 'pad-rename' %}/g"
find . -name '*.twig' -print0 | xargs -0 sed -i "s/{{ routes.padWrite }}/{% url 'pad-write' %}/g"

# Change file extention to ".html"
find . -name '*.twig' -print0 | xargs -0 sed -i 's/twig/html/g'
find . -type f -name '*.twig' | while read file; do
  rename .twig .html $file
done

mv 00-atoms atoms
mv 01-molecules molecules
mv 02-organisms organisms
mv 03-templates templates
mv 04-pages pages

# TODO:
